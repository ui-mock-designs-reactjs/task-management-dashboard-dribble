import React from 'react';
import Calendar from 'react-calendar';
import './App.css';
import 'react-calendar/dist/Calendar.css'
import logo from './img/disney-logo.png';
import cat from './img/cat.svg'
import { AreaChart, LineChart, Area, Line, XAxis, YAxis, CartesianGrid, Tooltip, ResponsiveContainer } from 'recharts';

function App() {
    const data = [
        {
            name: 'May',
            uv: 20,
            pv: 11,
            amt: 2400,
        },
        {
            name: 'Jun',
            uv: 11,
            pv: 6,
            amt: 2210,
        },
        {
            name: 'Jul',
            uv: 15,
            pv: 26,
            amt: 2290,
        },
        {
            name: 'Aug',
            uv: 7,
            pv: 10,
            amt: 2000,
        },
        {
            name: 'Sep',
            uv: 3,
            pv: 2,
            amt: 2181,
        },
    ];
  return (
    <div className="App bg-gray-200 p-2 flex font-montserrat" style={{
        height: "90vh"
    }}>
       <div className="design-container bg-white flex flex-1 rounded-lg">
           <div className="sidebar1 w-60 border-r">
               <div className="logo1 flex  h-14 w-full mt-6">
                   <img src={logo} alt="" className="object-contain w-48"/>
               </div>
               <div className="menu py-8 px-4">

                   <div className="menu-item mb-2 flex px-8 py-4 text-gray-500">
                   <div className="icon">
                       <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                           <path fillRule="evenodd" d="M10 9a3 3 0 100-6 3 3 0 000 6zm-7 9a7 7 0 1114 0H3z" clipRule="evenodd" />
                       </svg>
                   </div>
                       <div className="menu-text pl-4 text-sm font-bold">
                           Profile
                       </div>

                   </div>
                   <div className="menu-item mb-2 flex pl-8 pr-4 py-4 bg-gray-500 text-white  border rounded-lg items-center">
                       <div className="icon">
                           <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                               <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M19 11H5m14 0a2 2 0 012 2v6a2 2 0 01-2 2H5a2 2 0 01-2-2v-6a2 2 0 012-2m14 0V9a2 2 0 00-2-2M5 11V9a2 2 0 012-2m0 0V5a2 2 0 012-2h6a2 2 0 012 2v2M7 7h10" />
                           </svg>
                       </div>
                       <div className="menu-text pl-4 text-sm font-bold flex-1">
                           Tasks
                       </div>
                       <div className="icon ">
                           <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                               <path d="M10 6a2 2 0 110-4 2 2 0 010 4zM10 12a2 2 0 110-4 2 2 0 010 4zM10 18a2 2 0 110-4 2 2 0 010 4z" />
                           </svg>
                       </div>
                   </div>
                   <div className="menu-item mb-2 flex px-8 py-4 text-gray-500 ">
                       <div className="icon">
                           <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                               <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M12 6.253v13m0-13C10.832 5.477 9.246 5 7.5 5S4.168 5.477 3 6.253v13C4.168 18.477 5.754 18 7.5 18s3.332.477 4.5 1.253m0-13C13.168 5.477 14.754 5 16.5 5c1.747 0 3.332.477 4.5 1.253v13C19.832 18.477 18.247 18 16.5 18c-1.746 0-3.332.477-4.5 1.253" />
                           </svg>
                       </div>
                       <div className="menu-text pl-4 text-sm font-bold">
                           Planning
                       </div>

                   </div>
                   <div className="menu-item mb-2 flex px-8 py-4 text-gray-500 ">
                       <div className="icon">
                           <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                               <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 002.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 001.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 00-1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 00-2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 00-2.573-1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 00-1.065-2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 001.066-2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z" />
                               <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M15 12a3 3 0 11-6 0 3 3 0 016 0z" />
                           </svg>
                       </div>
                       <div className="menu-text text-sm pl-4 font-bold">
                           Settings
                       </div>

                   </div>
                   <div className="menu-item mb-2 flex px-8 py-4 text-gray-500 ">
                       <div className="icon">
                           <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                               <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M15 17h5l-1.405-1.405A2.032 2.032 0 0118 14.158V11a6.002 6.002 0 00-4-5.659V5a2 2 0 10-4 0v.341C7.67 6.165 6 8.388 6 11v3.159c0 .538-.214 1.055-.595 1.436L4 17h5m6 0v1a3 3 0 11-6 0v-1m6 0H9" />
                           </svg>
                       </div>
                       <div className="menu-text text-sm pl-4 font-bold">
                           Notifications
                       </div>

                   </div>
                   <div className="menu-item mb-2 flex px-8 py-4 text-gray-500 ">
                       <div className="icon">
                           <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                               <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z" />
                           </svg>
                       </div>
                       <div className="menu-text pl-4 text-sm font-bold">
                           People
                       </div>

                   </div>
               </div>
           </div>

           <div className="center flex-1 flex flex-col">
               <div className="center-heading p-8 text-3xl font-extrabold text-gray-500 leading-5 tracking-wider">
                   My tasks
               </div>
               <div className="top flex flex-1  items-stretch">

                   <div className="tl p-4 flex flex-col justify-between flex-1">

                       <img src="https://i.pinimg.com/originals/6e/d3/74/6ed374a0c1eeab3b6b34f2e9f1536cb1.png" className=""
                            alt=""/>
                   </div>
                   <div className="tr flex-1 pr-4">
                       <div className="font-bold text-lg px-4">May 2021</div>
                       <div className="calender">
                           <div className="days name">
                               <div>Mo</div>
                               <div>Tu</div>
                               <div>We</div>
                               <div>Th</div>
                               <div>Fr</div>
                               <div>Sa</div>
                               <div>Su</div>
                           </div>
                           <div className="days number">
                               <div>1</div>
                               <div>2</div>
                               <div>3</div>
                               <div>4</div>
                               <div>5</div>
                               <div>6</div>
                               <div>7</div>
                           </div>

                           <div className="days number">
                               <div>8</div>
                               <div>9</div>
                               <div className="today">10</div>
                               <div>11</div>
                               <div>12</div>
                               <div>13</div>
                               <div>14</div>
                           </div>

                           <div className="days number">
                               <div>15</div>
                               <div>16</div>
                               <div>17</div>
                               <div>18</div>
                               <div>19</div>
                               <div>20</div>
                               <div>21</div>
                           </div>

                           <div className="days number">
                               <div>22</div>
                               <div>23</div>
                               <div>24</div>
                               <div>25</div>
                               <div>26</div>
                               <div>27</div>
                               <div>28</div>
                           </div>

                           <div className="days number">
                               <div>29</div>
                               <div>30</div>
                               <div className="next-month">1</div>
                               <div className="next-month">2</div>
                               <div className="next-month">3</div>
                               <div className="next-month">4</div>
                               <div className="next-month">5</div>
                           </div>
                       </div>
                   </div>
               </div>
               <div className="bottom flex flex-1">
                   <div className="bl flex-1 flex flex-col">
                       <div className="font-bold text-lg px-8">Tasks activity</div>
                       <div className="flex-1 py-4">
                           <ResponsiveContainer>
                               <LineChart

                                   data={data}
                                   margin={{
                                       top: 0,
                                       right: 50,
                                       left: 0,
                                       bottom: 20,
                                   }}
                               >

                                   <XAxis axisLine={false} dataKey="name" tickLine={false} padding={{ left: 30 }} tick={{fill:"gray", fontSize:12}}  />
                                   <YAxis axisLine={false}  tickLine={false} padding={{ bottom: 30 }} tick={{fill:"gray", fontSize:12}} />
                                   <Tooltip />
                                   <Line type="monotone" dataKey="uv" stroke="#82ca9d" width={6} strokeWidth={3} />
                                   <Line type="monotone" dataKey="pv" stroke="#8884d8" width={6} strokeWidth={3} />
                                   <Area type="monotone" dataKey="uv" stroke="#8884d8" fill="#8884d8" />
                               </LineChart>
                           </ResponsiveContainer>
                       </div>
                   </div>
                   <div className="br flex-1">
                       <div className="font-bold text-lg px-8">In progress</div>
                       <div className="task-list p-2">
                           <div className="task-item p-4 flex items-center">
                               <div className="task-icon">
                                   <img src="https://www.shareicon.net/data/256x256/2016/08/18/808836_moon_512x512.png" className="w-12 h-12"
                                        alt=""/>
                               </div>
                               <div className="task-details pl-4 flex-1">
                                   <div className="task-title font-bold">Moodboard</div>
                                   <div className="task-title-secondary text-gray-500 text-sm">Second page</div>
                               </div>
                               <div className="task-icon text-gray-300">
                                   <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                       <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M9 5l7 7-7 7" />
                                   </svg>
                               </div>
                           </div>
                           <div className="task-item p-4 flex items-center">
                               <div className="task-icon">
                                   <img src="https://cdn.iconscout.com/icon/premium/png-256-thumb/new-moon-2941640-2432588.png" className="w-12 h-12"
                                        alt=""/>
                               </div>
                               <div className="task-details pl-4 flex-1">
                                   <div className="task-title font-bold">Rebranding</div>
                                   <div className="task-title-secondary text-gray-500 text-sm">Discuss the main idea</div>
                               </div>
                               <div className="task-icon text-gray-300">
                                   <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                       <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M9 5l7 7-7 7" />
                                   </svg>
                               </div>
                           </div>
                           <div className="task-item p-4 flex items-center">
                               <div className="task-icon">
                                   <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTLqmGySpXMD6D36AiNb_Pk9mvQNzX6NcLAJA&usqp=CAU" className="w-12 h-12"
                                        alt=""/>
                               </div>
                               <div className="task-details pl-4 flex-1">
                                   <div className="task-title font-bold">Book app</div>
                                   <div className="task-title-secondary text-gray-500 text-sm">Layout. illustrations</div>
                               </div>
                               <div className="task-icon text-gray-300">
                                   <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                       <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M9 5l7 7-7 7" />
                                   </svg>
                               </div>
                           </div>
                           <div className="task-item p-4 flex items-center">
                               <div className="task-icon border p-3 border rounded-2xl text-gray-500">
                                   <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                       <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
                                   </svg>
                               </div>
                               <div className="task-details pl-4 flex-1 text-gray-500">
                                   <div className="task-title font-extrabold">Add new task</div>

                               </div>

                           </div>
                       </div>
                   </div>
               </div>
           </div>
           <div className="chat w-80 flex flex-col border-l px-6">

               <div className="chat-header flex justify-between text-sm text-gray-400 px-2 py-4 items-center ">
                   <div className="chat-search flex ">
                       <span className="pr-2 font-bold text-lg">Search</span>
                       <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                           <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z" />
                       </svg></div>
                   <div className="chat-avatar flex items-center">
                       <svg xmlns="http://www.w3.org/2000/svg" className="h-4 w-4 m-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                           <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M19 9l-7 7-7-7" />
                       </svg>
                       <div className="avatar">
                           <img className="h-10 w-10 rounded-lg" src="https://randomuser.me/api/portraits/women/21.jpg" />
                       </div>
                   </div>
               </div>
               <div className="team-chat flex justify-between items-center py-4">
                   <div className="chat-title text-gray-800 px-4">
                       Team chat
                   </div>
                   <div className="chat-icons text-gray-400 flex">
                       <div className="phone-icon border border-4 rounded-lg border-gray-300 mx-1 p-2">
                           <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                               <path d="M2 3a1 1 0 011-1h2.153a1 1 0 01.986.836l.74 4.435a1 1 0 01-.54 1.06l-1.548.773a11.037 11.037 0 006.105 6.105l.774-1.548a1 1 0 011.059-.54l4.435.74a1 1 0 01.836.986V17a1 1 0 01-1 1h-2C7.82 18 2 12.18 2 5V3z" />
                           </svg>
                       </div>
                       <div className="phone-icon border border-4 rounded-lg border-gray-300 mx-1 p-2">
                           <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                               <path d="M2 6a2 2 0 012-2h6a2 2 0 012 2v8a2 2 0 01-2 2H4a2 2 0 01-2-2V6zM14.553 7.106A1 1 0 0014 8v4a1 1 0 00.553.894l2 1A1 1 0 0018 13V7a1 1 0 00-1.447-.894l-2 1z" />
                           </svg>
                       </div>
                   </div>
               </div>
               <div className="chat-messages px-2 flex-1 overflow-auto pt-2">
                   <div className="message pb-6">
                       <div className="message-top items-end flex">
                           <div className="avatar flex h-16 w-16 items-end">
                               <img className="rounded-lg" src="https://randomuser.me/api/portraits/women/6.jpg" />
                           </div>
                           <div className="message-text text-tiny py-4 px-4 ml-2 bg-purple-100 rounded-t-2xl rounded-br-2xl">
                               <div>Hi! Did you receive design edits? We need to do everything soon.</div>
                           </div>
                       </div>
                       <div className="message-footer flex justify-end text-xs text-gray-400 pt-1">
                           20 m ago
                       </div>
                   </div>
                   <div className="message pb-6">
                       <div className="message-top items-end flex flex-row-reverse">
                           <div className="avatar flex h-16 w-16 items-end">
                               <img className="rounded-lg" src="https://randomuser.me/api/portraits/women/2.jpg" />
                           </div>
                           <div className="message-text text-tiny py-4 px-4 mr-2 bg-blue-50 rounded-t-2xl rounded-bl-2xl">
                               <div>Hi! Did you receive design edits? We need to do everything soon.</div>
                           </div>
                       </div>
                       <div className="message-footer flex text-xs text-gray-400 pt-1">
                           20 m ago
                       </div>
                   </div>
                   <div className="message pb-6">
                       <div className="message-top items-end flex">
                           <div className="avatar flex h-16 w-16 items-end">
                               <img className="rounded-lg" src="https://randomuser.me/api/portraits/women/6.jpg" />
                           </div>
                           <div className="message-text text-tiny py-4 px-4 ml-2 bg-purple-100 rounded-t-2xl rounded-br-2xl">
                               <div>Hi! Did you receive design edits? We need to do everything soon.</div>
                           </div>
                       </div>
                       <div className="message-footer flex justify-end text-xs text-gray-400 pt-1">
                           20 m ago
                       </div>
                   </div>
                   <div className="message pb-6">
                       <div className="message-top items-end flex flex-row-reverse">
                           <div className="avatar flex h-16 w-16 items-end">
                               <img className="rounded-lg" src="https://randomuser.me/api/portraits/women/2.jpg" />
                           </div>
                           <div className="message-text text-tiny py-4 px-4 mr-2 bg-blue-50 rounded-t-2xl rounded-bl-2xl">
                               <div>Hi! Did you receive design edits? We need to do everything soon.</div>
                           </div>
                       </div>
                       <div className="message-footer flex text-xs text-gray-400 pt-1">
                           20 m ago
                       </div>
                   </div>
                   <div className="message pb-6">
                       <div className="message-top items-end flex">
                           <div className="avatar flex h-16 w-16 items-end">
                               <img className="rounded-lg" src="https://randomuser.me/api/portraits/women/6.jpg" />
                           </div>
                           <div className="message-text text-tiny py-4 px-4 ml-2 bg-purple-100 rounded-t-2xl rounded-br-2xl">
                               <div>Hi! Did you receive design edits? We need to do everything soon.</div>
                           </div>
                       </div>
                       <div className="message-footer flex justify-end text-xs text-gray-400 pt-1">
                           20 m ago
                       </div>
                   </div>
                   <div className="message pb-6">
                       <div className="message-top items-end flex flex-row-reverse">
                           <div className="avatar flex h-16 w-16 items-end">
                               <img className="rounded-lg" src="https://randomuser.me/api/portraits/women/2.jpg" />
                           </div>
                           <div className="message-text text-tiny py-4 px-4 mr-2 bg-blue-50 rounded-t-2xl rounded-bl-2xl">
                               <div>Hi! Did you receive design edits? We need to do everything soon.</div>
                           </div>
                       </div>
                       <div className="message-footer flex text-xs text-gray-400 pt-1">
                           20 m ago
                       </div>
                   </div>
                   <div className="message pb-6">
                       <div className="message-top items-end flex">
                           <div className="avatar flex h-16 w-16 items-end">
                               <img className="rounded-lg" src="https://randomuser.me/api/portraits/women/6.jpg" />
                           </div>
                           <div className="message-text text-tiny py-4 px-4 ml-2 bg-purple-100 rounded-t-2xl rounded-br-2xl">
                               <div>Hi! Did you receive design edits? We need to do everything soon.</div>
                           </div>
                       </div>
                       <div className="message-footer flex justify-end text-xs text-gray-400 pt-1">
                           20 m ago
                       </div>
                   </div>
                   <div className="message pb-6">
                       <div className="message-top items-end flex flex-row-reverse">
                           <div className="avatar flex h-16 w-16 items-end">
                               <img className="rounded-lg" src="https://randomuser.me/api/portraits/women/2.jpg" />
                           </div>
                           <div className="message-text text-tiny py-4 px-4 mr-2 bg-blue-50 rounded-t-2xl rounded-bl-2xl">
                               <div>Hi! Did you receive design edits? We need to do everything soon.</div>
                           </div>
                       </div>
                       <div className="message-footer flex text-xs text-gray-400 pt-1">
                           20 m ago
                       </div>
                   </div>
                   <div className="message pb-6">
                       <div className="message-top items-end flex">
                           <div className="avatar flex h-16 w-16 items-end">
                               <img className="rounded-lg" src="https://randomuser.me/api/portraits/women/6.jpg" />
                           </div>
                           <div className="message-text text-tiny py-4 px-4 ml-2 bg-purple-100 rounded-t-2xl rounded-br-2xl">
                               <div>Hi! Did you receive design edits? We need to do everything soon.</div>
                           </div>
                       </div>
                       <div className="message-footer flex justify-end text-xs text-gray-400 pt-1">
                           20 m ago
                       </div>
                   </div>
                   <div className="message pb-6">
                       <div className="message-top items-end flex flex-row-reverse">
                           <div className="avatar flex h-16 w-16 items-end">
                               <img className="rounded-lg" src="https://randomuser.me/api/portraits/women/2.jpg" />
                           </div>
                           <div className="message-text text-tiny py-4 px-4 mr-2 bg-blue-50 rounded-t-2xl rounded-bl-2xl">
                               <div>Hi! Did you receive design edits? We need to do everything soon.</div>
                           </div>
                       </div>
                       <div className="message-footer flex text-xs text-gray-400 pt-1">
                           20 m ago
                       </div>
                   </div>
                   <div className="message pb-6">
                       <div className="message-top items-end flex">
                           <div className="avatar flex h-16 w-16 items-end">
                               <img className="rounded-lg" src="https://randomuser.me/api/portraits/women/6.jpg" />
                           </div>
                           <div className="message-text text-tiny py-4 px-4 ml-2 bg-purple-100 rounded-t-2xl rounded-br-2xl">
                               <div>Hi! Did you receive design edits? We need to do everything soon.</div>
                           </div>
                       </div>
                       <div className="message-footer flex justify-end text-xs text-gray-400 pt-1">
                           20 m ago
                       </div>
                   </div>
                   <div className="message pb-6">
                       <div className="message-top items-end flex flex-row-reverse">
                           <div className="avatar flex h-16 w-16 items-end">
                               <img className="rounded-lg" src="https://randomuser.me/api/portraits/women/2.jpg" />
                           </div>
                           <div className="message-text text-tiny py-4 px-4 mr-2 bg-blue-50 rounded-t-2xl rounded-bl-2xl">
                               <div>Hi! Did you receive design edits? We need to do everything soon.</div>
                           </div>
                       </div>
                       <div className="message-footer flex text-xs text-gray-400 pt-1">
                           20 m ago
                       </div>
                   </div>
               </div>
               <div className="chat-footer flex justify-between bg-gray-100 p-4 mx-1 my-4 rounded-2xl">
                   <input className="outline-none bg-transparent" type="text" placeholder="Type here"/>
                   <div className="icon-send transform rotate-90">
                       <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                           <path d="M10.894 2.553a1 1 0 00-1.788 0l-7 14a1 1 0 001.169 1.409l5-1.429A1 1 0 009 15.571V11a1 1 0 112 0v4.571a1 1 0 00.725.962l5 1.428a1 1 0 001.17-1.408l-7-14z" />
                       </svg>
                   </div>
               </div>
           </div>
       </div>
    </div>
  );
}

export default App;
