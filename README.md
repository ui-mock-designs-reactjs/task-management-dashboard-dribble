# Project management dashboard along with team chat

## Tech

Here are the list of different technologies/frameworks/libraries used:
- [React with TypeScript](https://reactjs.org/) - For building the frontend components
- [React DnD](https://react-dnd.github.io/react-dnd/) - Add drag and drop functionality
- [TailwindCSS](https://tailwindcss.com/) - CSS utility framework to build designs easily and quickly

## Installation
Install the dependencies and start the server.

```sh
npm i
npm start
```
## Screenshots
![Screenshot](https://gitlab.com/ui-mock-designs-reactjs/task-management-dashboard-dribble/-/raw/master/Screenshot%201.gif?inline=false "Screenshot 1")